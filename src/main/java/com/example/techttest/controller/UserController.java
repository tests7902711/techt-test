package com.example.techttest.controller;

import com.example.techttest.adapter.UserAdapter;
import com.example.techttest.model.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/data")
@RequiredArgsConstructor
public class UserController {
    private final UserAdapter userAdapter;

    @Operation(summary = "Add user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User with returned key added",
                    content = { @Content(mediaType = "text/plain",
                            schema = @Schema(implementation = String.class)) }),
            @ApiResponse(responseCode = "409", description = "User with given key is already present",
                    content = @Content) })
    @PostMapping
    public Mono<String> addUser(@RequestBody @Valid User user) {
        return userAdapter.addUser(user)
                .map(res -> {
                    if (res) {
                        return String.valueOf(user.id());
                    } else {
                        throw new ResponseStatusException(HttpStatus.CONFLICT, "User with given key is already present");
                    }
                });
    }

    @Operation(summary = "Get user by key")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User founded",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = User.class)) }),
            @ApiResponse(responseCode = "404", description = "User with given key is absent",
                    content = @Content) })
    @GetMapping("/{key}")
    public Mono<User> getUser(@PathVariable String key) {
        return userAdapter.getUser(key)
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "User with given key is absent")));
    }

    @Operation(summary = "Update user by key")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User updated",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = User.class)) }),
            @ApiResponse(responseCode = "404", description = "User with given key is absent",
                    content = @Content) })
    @PutMapping("/{key}")
    public Mono<User> updateUser(@PathVariable String key, @RequestBody @Valid User user) {
        return userAdapter.updateUser(key, user)
                .map(res -> {
                    if (res) {
                        return user;
                    } else {
                        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with given key is absent");
                    }
                });
    }

    @Operation(summary = "Remove user by key")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User removed"),
            @ApiResponse(responseCode = "404", description = "User with given key is absent",
                    content = @Content) })
    @DeleteMapping("/{key}")
    public Mono<Void> removeUser(@PathVariable String key) {
        return userAdapter.removeUser(key)
                .doOnNext(res -> {
                    if (!res) {
                        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with given key is absent");
                    }
                })
                .then();
    }
}
