package com.example.techttest.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
public record User(
        @NotNull(message = "ID is mandatory")
        Integer id,
        @NotBlank(message = "firstName is mandatory")
        String firstName,
        @NotBlank(message = "lastName is mandatory")
        String lastName,
        Integer age) {
}
