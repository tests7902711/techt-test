package com.example.techttest;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Techt test", version = "1.0", description = "Documentation APIs v1.0"))
public class TechtTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechtTestApplication.class, args);
	}

}
