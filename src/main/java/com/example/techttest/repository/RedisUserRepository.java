package com.example.techttest.repository;

import com.example.techttest.model.User;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.core.ReactiveValueOperations;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public class RedisUserRepository implements UserRepository{
    private final ReactiveValueOperations<String, User> reactiveValueOps;

    public RedisUserRepository(ReactiveRedisTemplate<String, User> redisTemplate) {
        this.reactiveValueOps = redisTemplate.opsForValue();
    }

    @Override
    public Mono<Boolean> addUser(User user) {
        return reactiveValueOps.setIfAbsent(String.valueOf(user.id()), user)
                .log("RedisUserRepository addUser");
    }

    @Override
    public Mono<User> getUser(String key) {
        return reactiveValueOps.get(key)
                .log("RedisUserRepository getUser");
    }

    @Override
    public Mono<Boolean> updateUser(String key, User user) {
        return reactiveValueOps.setIfPresent(key, user)
                .log("RedisUserRepository updateUser");
    }

    @Override
    public Mono<Boolean> removeUser(String key) {
        return reactiveValueOps.delete(key)
                .log("RedisUserRepository removeUser");
    }
}
