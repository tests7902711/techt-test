package com.example.techttest.repository;

import com.example.techttest.model.User;
import reactor.core.publisher.Mono;

public interface UserRepository {
    Mono<Boolean> addUser(User user);

    Mono<User> getUser(String key);

    Mono<Boolean> updateUser(String key, User user);

    Mono<Boolean> removeUser(String key);
}
