package com.example.techttest.service;

import com.example.techttest.adapter.UserAdapter;
import com.example.techttest.model.User;
import com.example.techttest.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class UserService implements UserAdapter {
    private final UserRepository userRepository;

    @Override
    public Mono<Boolean> addUser(User user) {
        return userRepository.addUser(user);
    }

    @Override
    public Mono<User> getUser(String key) {
        return userRepository.getUser(key)
                .log();
    }

    @Override
    public Mono<Boolean> updateUser(String key, User user) {
        return userRepository.updateUser(key, user);
    }

    @Override
    public Mono<Boolean> removeUser(String key) {
        return userRepository.removeUser(key);
    }
}
